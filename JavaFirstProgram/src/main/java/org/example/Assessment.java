package org.example;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Assessment {

    public static void main(String[] args) throws IOException {
        System.out.println("Search student by:");
        System.out.println("1. Admission Number");
        System.out.println("2. Name");
        System.out.print("Enter your choice: ");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        ReadExcel printing = new ReadExcel();
        List<Student> students = printing.readDataFromExcel();
        switch (choice) {
            case 1 -> {
                System.out.print("Enter the Admission Number: ");
                double admissionNumber = scanner.nextDouble();
                searchByAdmissionNumber(students, admissionNumber);
            }
            case 2 -> {
                System.out.print("Enter the Name: ");
                String name = scanner.next().toLowerCase();
                searchByName(students, name);
            }
            default -> System.out.println("Invalid choice.");
        }

        scanner.close();
    }

    private static void searchByAdmissionNumber(List<Student> students, double admissionNumber) {
        for (Student student : students) {
            if (student.getAdmission() == admissionNumber) {
                printStudentDetails(student);
                return;
            }
        }
        System.out.println("No student found with the given admission number.");
    }

    private static void searchByName(List<Student> students, String name) {
        boolean found = false;
        for (Student student : students) {
            if (student.getName().equalsIgnoreCase(name)) {
                printStudentDetails(student);
                found = true;
            }
        }
        if (!found) {
            System.out.println("No student found with the given name.");
        }
    }

    private static void printStudentDetails(Student student) {
        gradeAssignment gradeAssign = new gradeAssignment();
        System.out.println("Name: " + student.getName());
        System.out.println("Admission No: " + student.getAdmission());
        System.out.println("Percentage: " + student.getPercentage());

        System.out.println("Physics:");
        System.out.println("\tMark: " + student.getphysicsMark());
        System.out.println("\tGrade: " + gradeAssign.grade(student.getphysicsMark()));
        System.out.println("\tGrade Point: " + gradeAssign.point(student.getphysicsMark()));

        System.out.println("Chemistry:");
        System.out.println("\tMarks: " + student.getchemistryMark());
        System.out.println("\tGrade: " + gradeAssign.grade(student.getchemistryMark()));
        System.out.println("\tGrade Point: " + gradeAssign.point(student.getchemistryMark()));

        System.out.println("Maths:");
        System.out.println("\tMarks: " + student.getmathematicsMark());
        System.out.println("\tGrade: " + gradeAssign.grade(student.getmathematicsMark()));
        System.out.println("\tGrade Point: " + gradeAssign.point(student.getmathematicsMark()));

        System.out.println();
    }
}

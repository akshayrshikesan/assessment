package org.example;
public class gradeAssignment {
    String Grade;
    double pointer;

    public String grade(double marks) {
        if(marks>90) {
            Grade="A1";
        }
        else if(marks>80) {
            Grade="A2";
        }
        else if(marks>70) {
            Grade="B1";
        }
        else if(marks>60) {
            Grade="B2";
        }
        else if(marks>50) {
            Grade="C1";
        }
        else if(marks>40) {
            Grade="C2";
        }
        else if(marks>32 ) {
            Grade="D1";
        }
        else if(marks>20) {
            Grade="E1";
        }
        else{
            Grade="E2";
        }

        return Grade;

    }
    public double point(double Grade1) {
        if(Grade1>90) {
            pointer=10;
        }
        else if(Grade1>80){
            pointer=9;
        }
        else if(Grade1>70){
            pointer=8;
        }
        else if(Grade1>60){
            pointer=7;
        }
        else if(Grade1>50){
            pointer=6;
        }
        else if(Grade1>40){
            pointer=5;
        }
        else if(Grade1>32){
            pointer=4;
        }
        else if(Grade1>20){
            pointer=0 ;
        }
        return pointer;
    }
}


package org.example;
public class Student {
    String name;
    double admissionNumber;
    double physicsMark;
    double chemistryMark;
    double mathematicsMark;
    double marksum;
    double percentage;

    public Student(double admissionNumber, String name, double physicsMark, double chemistryMark, double mathematicsMark) {
        this.admissionNumber = admissionNumber;
        this.name = name;
        this.physicsMark = physicsMark;
        this.chemistryMark = chemistryMark;
        this.mathematicsMark = mathematicsMark;
        setTotalMark();
        setPercentage();
    }
    public void setTotalMark()
    {
        this.marksum= this.physicsMark+ this.chemistryMark + this.mathematicsMark ;
    }
    public void setPercentage()
    {
        this.percentage=(this.marksum/300)*100;
    }
    public String getName()
    {
        return name;
    }
    public double getAdmission()
    {
        return admissionNumber;
    }
    public double getphysicsMark()
    {
        return physicsMark;
    }
    public double getchemistryMark()
    {
        return chemistryMark;
    }
    public double getmathematicsMark()
    {
        return mathematicsMark;
    }
    public double getPercentage()
    {
        return percentage;
    }

}


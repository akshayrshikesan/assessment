package org.example;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadExcel {
    public List<Student> readDataFromExcel() throws IOException {
        List<Student> students = new ArrayList<>();
        String fis = "Student.xlsx";
        FileInputStream fileInputStream = new FileInputStream(fis);
        XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
        XSSFSheet sheet = wb.getSheet("sheet1");

        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            double admissionNumber = sheet.getRow(i).getCell(0).getNumericCellValue();
            String name = sheet.getRow(i).getCell(1).getStringCellValue();
            double physicsMark = sheet.getRow(i).getCell(2).getNumericCellValue();
            double chemistryMark = sheet.getRow(i).getCell(3).getNumericCellValue();
            double mathematicsMark = sheet.getRow(i).getCell(4).getNumericCellValue();

            Student student = new Student(admissionNumber, name, physicsMark, chemistryMark, mathematicsMark);
            students.add(student);
        }

        wb.close();
        fileInputStream.close();

        return students;
    }
}



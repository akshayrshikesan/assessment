class Student {
    private String AdmissionNumber;
    private String Name;
    private double Physics;
    private double Chemistry;
    private double Maths;

    public Student(String AdmissionNumber, String Name, double Physics, double Chemistry, double Maths) {
        this.AdmissionNumber = AdmissionNumber;
        this.Name = Name;
        this.Physics = Physics;
        this.Chemistry = Chemistry;
        this.Maths = Maths;
    }

    public String getAdmissionNumber() {
        return AdmissionNumber;
    }

    public String getName() {
        return Name;
    }

    public double getPhysics() {
        return Physics;
    }

    public double getChemistry() {
        return Chemistry;
    }

    public double getMaths() {
        return Maths;
    }
}
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.List;
public class Main {
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/assesment";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    public static void main(String[] args) {
        List<Student> studentList = readExcelData("Student.xlsx");

        if (studentList.isEmpty()) {
            System.out.println("Error reading Excel file or Empty Excel.");
            return;
        }
        InsertOrUpdate(studentList);
        Scanner scanner = new Scanner(System.in);
            System.out.println("Choose an option:");
            System.out.println("1. Search by Admission Number");
            System.out.println("2. Search by Name");
            System.out.print("Enter your choice: ");
            String choice = scanner.nextLine();

            switch (choice) {
                case "1":
                    System.out.print("Enter Admission Number: ");
                    String AdmissionNumber = scanner.nextLine().toUpperCase();
                    search(AdmissionNumber, "AdmissionNumber");
                    break;
                case "2":
                    System.out.print("Enter Name: ");
                    String Name = scanner.nextLine().toUpperCase();
                    search(Name, "Name");
                    break;
                default:
                    System.out.println("Invalid choice. Please enter a valid option.");
            }
            scanner.close();
    }

    public static List<Student> readExcelData(String filename) {
        List<Student> studentList = new ArrayList<>();
        String jsonFileName = "student.json";
        try (FileInputStream fis = new FileInputStream(new File(filename));
             Workbook workbook = new XSSFWorkbook(fis);
             FileWriter writer = new FileWriter(jsonFileName);) {
            Sheet sheet = workbook.getSheetAt(0);
            JSONArray jsonArray = new JSONArray();

            for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
                JSONObject studentJson = new JSONObject();
                String admissionNumber = sheet.getRow(rowIndex).getCell(0).getStringCellValue();
                String name = sheet.getRow(rowIndex).getCell(1).getStringCellValue();
                double physics = sheet.getRow(rowIndex).getCell(2).getNumericCellValue();
                double chemistry = sheet.getRow(rowIndex).getCell(3).getNumericCellValue();
                double maths = sheet.getRow(rowIndex).getCell(4).getNumericCellValue();
                studentJson.put("AdmissionNumber", admissionNumber);
                studentJson.put("Name", name);
                studentJson.put("Physics", physics);
                studentJson.put("Chemistry", chemistry);
                studentJson.put("Maths", maths);
                jsonArray.put(studentJson);
                Student student = new Student(admissionNumber, name, physics, chemistry, maths);
                studentList.add(student);


            }
            writer.write(jsonArray.toString(4));
            System.out.println("Data written to " + jsonFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return studentList;
    }
    public static void InsertOrUpdate(List<Student> studentList) {
        try (Connection connection = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASSWORD)) {
            for (Student student : studentList) {
                String admissionNo = student.getAdmissionNumber();
                if (isStudentInDatabase(connection, admissionNo)) {
                    updateStudent(connection, student);
                } else {
                    insertStudent(connection, student);
                }
            }
            System.out.println("Student data inserted/updated in the database.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean isStudentInDatabase(Connection connection, String admissionNumber) throws SQLException {
        String selectQuery = "SELECT AdmissionNumber FROM student WHERE AdmissionNumber = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(selectQuery)) {
            preparedStatement.setString(1, admissionNumber);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        }
    }

    public static void insertStudent(Connection connection, Student student) throws SQLException {
        String insertQuery = "INSERT INTO student (AdmissionNumber, Name, Physics, Chemistry, Maths) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(insertQuery)) {
            preparedStatement.setString(1, student.getAdmissionNumber());
            preparedStatement.setString(2, student.getName());
            preparedStatement.setDouble(3, student.getPhysics());
            preparedStatement.setDouble(4, student.getChemistry());
            preparedStatement.setDouble(5, student.getMaths());
            preparedStatement.executeUpdate();
        }
    }

    public static void updateStudent(Connection connection, Student student) throws SQLException {
        String updateQuery = "UPDATE student SET Name = ?, Physics = ?, Chemistry = ?, Maths = ? WHERE AdmissionNumber = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(updateQuery)) {
            preparedStatement.setString(1, student.getName());
            preparedStatement.setDouble(2, student.getPhysics());
            preparedStatement.setDouble(3, student.getChemistry());
            preparedStatement.setDouble(4, student.getMaths());
            preparedStatement.setString(5, student.getAdmissionNumber());
            preparedStatement.executeUpdate();
        }
    }

    public static void search(String searchKey, String keyType) {
        try (Connection connection = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASSWORD)) {
            String selectQuery = "SELECT * FROM student WHERE " + keyType + " = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
            preparedStatement.setString(1, searchKey);
            ResultSet resultSet = preparedStatement.executeQuery();

            JSONArray jsonArray = new JSONArray();
            while (resultSet.next()) {
                JSONObject studentJson = new JSONObject();
                studentJson.put("AdmissionNumber", resultSet.getString("AdmissionNumber"));
                studentJson.put("Name", resultSet.getString("Name"));
                studentJson.put("Physics", resultSet.getDouble("Physics"));
                studentJson.put("Chemistry", resultSet.getDouble("Chemistry"));
                studentJson.put("Maths", resultSet.getDouble("Maths"));
                jsonArray.put(studentJson);
            }

            if (jsonArray.isEmpty()) {
                System.out.println("No matching students found");
            } else {
                System.out.println(jsonArray.toString(4));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
